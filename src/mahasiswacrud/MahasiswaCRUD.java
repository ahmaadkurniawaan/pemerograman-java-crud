/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mahasiswacrud;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author ahmadkurniawan
 */
public class MahasiswaCRUD {

    private static Connection MYSQLConfig;
        
        
    public static Connection ConfigDB() throws SQLException{
        
        try{
            String url = "jdbc:mysql://localhost:3306/belajar_crud";
            String user = "root";
            String password = "";

            
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            MYSQLConfig = DriverManager.getConnection(url,user,password);
            
        }catch(SQLException e){
        
            System.out.println("Koneksi Database Gagal !!! " + e.getMessage());
        
    }
        
        return MYSQLConfig;
        
    }   
    

    
}
